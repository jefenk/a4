import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


//https://www.baeldung.com/java-hashcode
//https://ru.wikipedia.org/wiki/Кватернион
//Regex - https://stackoverflow.com/questions/50528957/regex-to-match-scientific-notation-of-numbers-but-not-other-letters
//Regex tester - https://regex101.com/

/**
 * Quaternions. Basic operations.
 */
public class Quaternion {

    private static final double eqIndex = 0.000001;
    private final double real;
    private final double partI;
    private final double partJ;
    private final double partK;


    /**
     * Constructor from four double values.
     *
     * @param a real part
     * @param b imaginary part i
     * @param c imaginary part j
     * @param d imaginary part k
     */
    public Quaternion(double a, double b, double c, double d) {
        this.real = a;
        this.partI = b;
        this.partJ = c;
        this.partK = d;
    }

    /**
     * Real part of the quaternion.
     *
     * @return real part
     */
    public double getRpart() {
        return real;
    }

    /**
     * Imaginary part i of the quaternion.
     *
     * @return imaginary part i
     */
    public double getIpart() {
        return partI;
    }

    /**
     * Imaginary part j of the quaternion.
     *
     * @return imaginary part j
     */
    public double getJpart() {
        return partJ;
    }

    /**
     * Imaginary part k of the quaternion.
     *
     * @return imaginary part k
     */
    public double getKpart() {
        return partK;
    }

    /**
     * Conversion of the quaternion to the string.
     *
     * @return a string form of this quaternion:
     * "a+bi+cj+dk"
     * (without any brackets)
     */
    @Override
    public String toString() {
        DecimalFormat df = (DecimalFormat)NumberFormat.getNumberInstance(Locale.US);
        String string = "" + df.format(this.real);
        if (this.partI >= 0) {
            string = string + "+";
        }
        string = string + df.format(this.partI) + "i";
        if (this.partJ >= 0) {
            string = string + "+";
        }
        string = string + df.format(this.partJ) + "j";
        if (this.partK >= 0) {
            string = string + "+";
        }
        string = string + df.format(this.partK) + "k";

        return string;
    }

    /**
     * Conversion from the string to the quaternion.
     * Reverse to <code>toString</code> method.
     *
     * @param s string of form produced by the <code>toString</code> method
     * @return a quaternion represented by string s
     * @throws IllegalArgumentException if string s does not represent
     *                                  a quaternion (defined by the <code>toString</code> method)
     */
    public static Quaternion valueOf(String s) {
        Pattern pattern = Pattern.compile("^[+-]?\\d+(?:\\.\\d*(?:[eE][+-]?\\d+)?)?[+-]?\\d+(?:\\.\\d*(?:[eE][+-]?\\d+)?)?[iI][+-]?\\d+(?:\\.\\d*(?:[eE][+-]?\\d+)?)?[jJ][+-]?\\d+(?:\\.\\d*(?:[eE][+-]?\\d+)?)?[kK]");
        Matcher matcher = pattern.matcher(s);

        if(!(matcher.matches())) {
            throw new RuntimeException("The string doesn't match Quaternion pattern\n" +
                    "Please check if decimal delimiter is '.' .\n" +
                    "Please check if all string has all parts and imaginary parts are indexed by i, j and k.\n" +
                    "Your string was " + s);
        }


        List<Double> quaternionParams = new ArrayList<>();

        String[] sArr = s.split("");
        for (int i = 1; i < sArr.length; i++) {
            if (sArr[i].equals("-") && !(sArr[i-1].equals("e"))) {
                sArr[i] = "+-";
            }
        }
        String newS = String.join("", sArr);

        StringTokenizer tokenizer = new StringTokenizer(newS, "+");
        while (tokenizer.hasMoreElements()) {
            String token = (String) tokenizer.nextElement();
            if (Character.isAlphabetic(token.charAt(token.length() - 1))) {
                token = token.substring(0, token.length() - 1);
            }
            try {
                quaternionParams.add(Double.parseDouble(token));
            } catch (NumberFormatException exception) {
                throw new NumberFormatException("Number format must be with '.' char. You have: " + s);
            }
        }

        return new Quaternion(quaternionParams.get(0),
                quaternionParams.get(1),
                quaternionParams.get(2),
                quaternionParams.get(3));
    }

    /**
     * Clone of the quaternion.
     *
     * @return independent clone of <code>this</code>
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Quaternion(this.real, this.partI, this.partJ, this.partK);
    }

    /**
     * Test whether the quaternion is zero.
     *
     * @return true, if the real part and all the imaginary parts are (close to) zero
     */
    public boolean isZero() {
        return Math.abs(0 - this.real) < eqIndex
                && Math.abs(0 - this.partI) < eqIndex
                && Math.abs(0 - this.partJ) < eqIndex
                && Math.abs(0 - this.partK) < eqIndex;
    }

    /**
     * Conjugate of the quaternion. Expressed by the formula
     * conjugate(a+bi+cj+dk) = a-bi-cj-dk
     *
     * @return conjugate of <code>this</code>
     */
    public Quaternion conjugate() {
        return new Quaternion(this.real, -this.partI, -this.partJ, -this.partK);
    }

    /**
     * Opposite of the quaternion. Expressed by the formula
     * opposite(a+bi+cj+dk) = -a-bi-cj-dk
     *
     * @return quaternion <code>-this</code>
     */
    public Quaternion opposite() {
        return new Quaternion(-this.real, -this.partI, -this.partJ, -this.partK);
    }

    /**
     * Sum of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
     *
     * @param q addend
     * @return quaternion <code>this+q</code>
     */
    public Quaternion plus(Quaternion q) {
        return new Quaternion(this.real + q.real,
                this.partI + q.partI,
                this.partJ + q.partJ,
                this.partK + q.partK);
    }

    /**
     * Product of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
     * (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
     *
     * @param q factor
     * @return quaternion <code>this*q</code>
     */
    public Quaternion times(Quaternion q) {
        return new Quaternion(this.real * q.real - this.partI * q.partI - this.partJ * q.partJ - this.partK * q.partK,
                this.real * q.partI + q.real * this.partI + this.partJ * q.partK - this.partK * q.partJ,
                this.real * q.partJ - this.partI * q.partK + this.partJ * q.real + this.partK * q.partI,
                this.real * q.partK + this.partI * q.partJ - this.partJ * q.partI + this.partK * q.real);
    }

    /**
     * Multiplication by a coefficient.
     *
     * @param r coefficient
     * @return quaternion <code>this*r</code>
     */
    public Quaternion times(double r) {
        return new Quaternion(this.real * r, this.partI * r, this.partJ * r, this.partK * r);
    }

    /**
     * Inverse of the quaternion. Expressed by the formula
     * 1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
     * ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
     *
     * @return quaternion <code>1/this</code>
     */
    public Quaternion inverse() {
        if (this.isZero()) {
            throw new RuntimeException("The division by Zero is impossible!");
        }
        double sumOfPows = Math.pow(this.real, 2) + Math.pow(this.partI, 2) + Math.pow(this.partJ, 2) + Math.pow(this.partK, 2);

        return new Quaternion(this.real / sumOfPows, -this.partI / sumOfPows, -this.partJ / sumOfPows, -this.partK / sumOfPows);

    }

    /**
     * Difference of quaternions. Expressed as addition to the opposite.
     *
     * @param q subtrahend
     * @return quaternion <code>this-q</code>
     */
    public Quaternion minus(Quaternion q) {
        return new Quaternion(this.real - q.real, this.partI - q.partI, this.partJ - q.partJ, this.partK - q.partK);
    }

    /**
     * Right quotient of quaternions. Expressed as multiplication to the inverse.
     *
     * @param q (right) divisor
     * @return quaternion <code>this*inverse(q)</code>
     */
    public Quaternion divideByRight(Quaternion q) {
        if (q.isZero()) {
            throw new RuntimeException("The division by Zero is impossible!");
        }
        return this.times(q.inverse());
    }

    /**
     * Left quotient of quaternions.
     *
     * @param q (left) divisor
     * @return quaternion <code>inverse(q)*this</code>
     */
    public Quaternion divideByLeft(Quaternion q) {
        if (q.isZero()) {
            throw new RuntimeException("The division by Zero is impossible!");
        }
        return q.inverse().times(this);
    }

    /**
     * Equality test of quaternions. Difference of equal numbers
     * is (close to) zero.
     *
     * @param qo second quaternion
     * @return logical value of the expression <code>this.equals(qo)</code>
     */
    @Override
    public boolean equals(Object qo) {
        if (getClass() != qo.getClass()) {
            return false;
        }
        return Math.abs(this.real - ((Quaternion) qo).real) < eqIndex
                && Math.abs(this.partI - ((Quaternion) qo).partI) < eqIndex
                && Math.abs(this.partJ - ((Quaternion) qo).partJ) < eqIndex
                && Math.abs(this.partK - ((Quaternion) qo).partK) < eqIndex;
    }

    /**
     * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
     *
     * @param q factor
     * @return dot product of this and q
     */
    public Quaternion dotMult(Quaternion q) {
        Quaternion quaternion = this.times(q.conjugate()).plus(q.times(this.conjugate()));
        return new Quaternion(quaternion.real / 2, quaternion.partI / 2, quaternion.partJ / 2, quaternion.partK / 2);
    }

    /**
     * Integer hashCode has to be the same for equal objects.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.real, this.partI, this.partJ, this.partK);
    }

    /**
     * Norm of the quaternion. Expressed by the formula
     * norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
     *
     * @return norm of <code>this</code> (norm is a real number)
     */
    public double norm() {
        return Math.sqrt(Math.pow(this.real, 2) + Math.pow(this.partI, 2) + Math.pow(this.partJ, 2) + Math.pow(this.partK, 2));
    }

    /**
     * Main method for testing purposes.
     *
     * @param arg command line parameters
     */
    public static void main(String[] arg) {
        Quaternion arv1 = new Quaternion(1e-2, 1, 1, 1);
        if (arg.length > 0)
            arv1 = valueOf(arg[0]);
        System.out.println("first: " + arv1);
        System.out.println("real: " + arv1.getRpart());
        System.out.println("imagi: " + arv1.getIpart());
        System.out.println("imagj: " + arv1.getJpart());
        System.out.println("imagk: " + arv1.getKpart());
        System.out.println("isZero: " + arv1.isZero());
        System.out.println("conjugate: " + arv1.conjugate());
        System.out.println("opposite: " + arv1.opposite());
        System.out.println("hashCode: " + arv1.hashCode());
        Quaternion res = null;
        try {
            res = (Quaternion) arv1.clone();
        } catch (CloneNotSupportedException ignored) {
        }
        assert res != null;
        System.out.println("clone equals to original: " + res.equals(arv1));
        System.out.println("clone is not the same object: " + (res != arv1));
        System.out.println("hashCode: " + res.hashCode());
        res = valueOf(arv1.toString());
        System.out.println("string conversion equals to original: "
                + res.equals(arv1));
        Quaternion arv2 = new Quaternion(1, -2., -1., 2.);
        if (arg.length > 1)
            arv2 = valueOf(arg[1]);
        System.out.println("second: " + arv2);
        System.out.println("hashCode: " + arv2.hashCode());
        System.out.println("equals: " + arv1.equals(arv2));
        res = arv1.plus(arv2);
        System.out.println("plus: " + res);
        System.out.println("times: " + arv1.times(arv2));
        System.out.println("minus: " + arv1.minus(arv2));
        double mm = arv1.norm();
        System.out.println("norm: " + mm);
        System.out.println("inverse: " + arv1.inverse());
        System.out.println("divideByRight: " + arv1.divideByRight(arv2));
        System.out.println("divideByLeft: " + arv1.divideByLeft(arv2));
        System.out.println("dotMult: " + arv1.dotMult(arv2));
    }
}
// end of file